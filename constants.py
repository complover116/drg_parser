import json

level_breakpoints = [
    0,
    3000,
    7000,
    12000,
    18000,
    25000,
    33000,
    42000,
    52000,
    63000,
    75000,
    88000,
    102000,
    117000,
    132500,
    148500,
    165000,
    182000,
    199500,
    217500,
    236000,
    255000,
    274500,
    294500,
    315000,
    999999999999999999999
]

mineral_names = ["Credits", "Jadiz", "Bismor", "Magnite", "Umanite", "Enor Pearl", "Croppa"]
mineral_colors = ["${color yellow}", "${color #00d310}", "${color #ffe008}", "${color #e94002}", "${color #5ae551}", "${color #f7f7f7}", "${color #62cea7}"]

promotion_costs = json.loads("""{"driller": [{"Credits": 8919, "Jadiz": 66, "Umanite": 65, "Enor Pearl": 63}, {"Credits": 11595, "Jadiz": 86, "Umanite": 85, "Enor Pearl": 82}, {"Credits": 15162, "Jadiz": 112, "Umanite": 111, "Enor Pearl": 107}, {"Credits": 19622, "Jadiz": 145, "Umanite": 143, "Enor Pearl": 139}, {"Credits": 25508, "Jadiz": 189, "Umanite": 186, "Enor Pearl": 180}, {"Credits": 33179, "Jadiz": 246, "Umanite": 242, "Enor Pearl": 234}, {"Credits": 42990, "Jadiz": 318, "Umanite": 313, "Enor Pearl": 304}, {"Credits": 56011, "Jadiz": 414, "Umanite": 408, "Enor Pearl": 396}, {"Credits": 69390, "Jadiz": 513, "Umanite": 506, "Enor Pearl": 490}, {"Credits": 83839, "Jadiz": 594, "Umanite": 585, "Enor Pearl": 567}, {"Credits": 96325, "Jadiz": 607, "Umanite": 598, "Enor Pearl": 580}, {"Credits": 107028, "Jadiz": 620, "Umanite": 611, "Enor Pearl": 592}, {"Credits": 115947, "Jadiz": 634, "Umanite": 624, "Enor Pearl": 605}, {"Credits": 123974, "Jadiz": 647, "Umanite": 637, "Enor Pearl": 617}, {"Credits": 131109, "Jadiz": 660, "Umanite": 650, "Enor Pearl": 630}, {"Credits": 137353, "Jadiz": 673, "Umanite": 663, "Enor Pearl": 643}, {"Credits": 142704, "Jadiz": 686, "Umanite": 676, "Enor Pearl": 655}, {"Credits": 147164, "Jadiz": 700, "Umanite": 689, "Enor Pearl": 668}], "engineer": [{"Credits": 8919, "Croppa": 64, "Enor Pearl": 63, "Bismor": 68}, {"Credits": 11595, "Croppa": 83, "Enor Pearl": 82, "Bismor": 88}, {"Credits": 15162, "Croppa": 109, "Enor Pearl": 107, "Bismor": 116}, {"Credits": 19622, "Croppa": 141, "Enor Pearl": 139, "Bismor": 150}, {"Credits": 25508, "Croppa": 183, "Enor Pearl": 180, "Bismor": 194}, {"Credits": 33179, "Croppa": 238, "Enor Pearl": 234, "Bismor": 253}, {"Credits": 42990, "Croppa": 308, "Enor Pearl": 304, "Bismor": 328}, {"Credits": 56011, "Croppa": 402, "Enor Pearl": 396, "Bismor": 427}, {"Credits": 69390, "Croppa": 498, "Enor Pearl": 490, "Bismor": 529}, {"Credits": 83839, "Croppa": 576, "Enor Pearl": 567, "Bismor": 612}, {"Credits": 96325, "Croppa": 589, "Enor Pearl": 580, "Bismor": 626}, {"Credits": 107028, "Croppa": 602, "Enor Pearl": 592, "Bismor": 639}, {"Credits": 115947, "Croppa": 614, "Enor Pearl": 605, "Bismor": 653}, {"Credits": 123974, "Croppa": 627, "Enor Pearl": 617, "Bismor": 666}, {"Credits": 131109, "Croppa": 640, "Enor Pearl": 630, "Bismor": 680}, {"Credits": 137353, "Croppa": 653, "Enor Pearl": 643, "Bismor": 694}, {"Credits": 142704, "Croppa": 666, "Enor Pearl": 655, "Bismor": 707}, {"Credits": 147164, "Croppa": 678, "Enor Pearl": 668, "Bismor": 721}], "gunner": [{"Credits": 8921, "Magnite": 66, "Umanite": 66, "Bismor": 63}, {"Credits": 11597, "Magnite": 86, "Umanite": 86, "Bismor": 82}, {"Credits": 15166, "Magnite": 112, "Umanite": 112, "Bismor": 107}, {"Credits": 19626, "Magnite": 145, "Umanite": 145, "Bismor": 139}, {"Credits": 25514, "Magnite": 189, "Umanite": 189, "Bismor": 180}, {"Credits": 33186, "Magnite": 246, "Umanite": 246, "Bismor": 234}, {"Credits": 42999, "Magnite": 318, "Umanite": 318, "Bismor": 304}, {"Credits": 56024, "Magnite": 414, "Umanite": 414, "Bismor": 396}, {"Credits": 69405, "Magnite": 513, "Umanite": 513, "Bismor": 490}, {"Credits": 83857, "Magnite": 594, "Umanite": 594, "Bismor": 567}, {"Credits": 96347, "Magnite": 607, "Umanite": 607, "Bismor": 580}, {"Credits": 107052, "Magnite": 620, "Umanite": 620, "Bismor": 592}, {"Credits": 115973, "Magnite": 634, "Umanite": 634, "Bismor": 605}, {"Credits": 124002, "Magnite": 647, "Umanite": 647, "Bismor": 617}, {"Credits": 131139, "Magnite": 660, "Umanite": 660, "Bismor": 630}, {"Credits": 137383, "Magnite": 673, "Umanite": 673, "Bismor": 643}, {"Credits": 142736, "Magnite": 686, "Umanite": 686, "Bismor": 655}, {"Credits": 147197, "Magnite": 700, "Umanite": 700, "Bismor": 668}], "scout": [{"Credits": 8922, "Croppa": 64, "Jadiz": 69, "Magnite": 62}, {"Credits": 11599, "Croppa": 83, "Jadiz": 90, "Magnite": 81}, {"Credits": 15167, "Croppa": 109, "Jadiz": 117, "Magnite": 105}, {"Credits": 19628, "Croppa": 141, "Jadiz": 152, "Magnite": 136}, {"Credits": 25517, "Croppa": 183, "Jadiz": 197, "Magnite": 177}, {"Credits": 33190, "Croppa": 238, "Jadiz": 257, "Magnite": 231}, {"Credits": 43004, "Croppa": 308, "Jadiz": 333, "Magnite": 299}, {"Credits": 56030, "Croppa": 402, "Jadiz": 433, "Magnite": 389}, {"Credits": 69413, "Croppa": 498, "Jadiz": 537, "Magnite": 482}, {"Credits": 83867, "Croppa": 576, "Jadiz": 621, "Magnite": 558}, {"Credits": 96358, "Croppa": 589, "Jadiz": 635, "Magnite": 570}, {"Credits": 107064, "Croppa": 602, "Jadiz": 649, "Magnite": 583}, {"Credits": 115986, "Croppa": 614, "Jadiz": 662, "Magnite": 595}, {"Credits": 124016, "Croppa": 627, "Jadiz": 676, "Magnite": 608}, {"Credits": 131153, "Croppa": 640, "Jadiz": 690, "Magnite": 620}, {"Credits": 137399, "Croppa": 653, "Jadiz": 704, "Magnite": 632}, {"Credits": 142752, "Croppa": 666, "Jadiz": 718, "Magnite": 645}, {"Credits": 147213, "Croppa": 678, "Jadiz": 731, "Magnite": 657}]}""")

ingredient_names = ["Barley Bulb", "Yeast Cone", "Malt Star", "Starch Nut"]
ingredient_colors = ["${color #e4413c}", "${color #3c5b8e}", "${color #5ebd98}", "${color #e247c4}"]
