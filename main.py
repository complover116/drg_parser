import struct
import sys
import datetime
import os
import constants


def format_timedelta(td):
    hours = td.seconds // 3600
    minutes = (td.seconds - hours * 3600) // 60
    return str(td.days) + " days, " + str(hours) + "h " + str(minutes) + "m"


class DRGSaveFile:
    def __init__(self, path):
        self.path = path
        with open(self.path, mode='rb') as drg_file:
            self.binary = drg_file.read()
        self.last_modified = datetime.datetime.fromtimestamp(os.path.getmtime(self.path), datetime.timezone.utc)
        self.drg_properties = {
            "perk_points": {
                "after_string": "PerkPoints",
                "offset": 26,
                "type": "int32"
            },
            "gunner_xp": {
                "after_string": "XP\0",
                "offset": 25,
                "type": "int32",
                "occurrence": 2
            },
            "driller_xp": {
                "after_string": "XP\0",
                "offset": 25,
                "type": "int32",
                "occurrence": 3
            },
            "engineer_xp": {
                "after_string": "XP\0",
                "offset": 25,
                "type": "int32",
                "occurrence": 4
            },
            "scout_xp": {
                "after_string": "XP\0",
                "offset": 25,
                "type": "int32",
                "occurrence": 5
            },
            "gunner_stars": {
                "after_string": "TimesRetired",
                "offset": 26,
                "type": "int32",
                "occurrence": 1
            },
            "driller_stars": {
                "after_string": "TimesRetired",
                "offset": 26,
                "type": "int32",
                "occurrence": 2
            },
            "engineer_stars": {
                "after_string": "TimesRetired",
                "offset": 26,
                "type": "int32",
                "occurrence": 3
            },
            "scout_stars": {
                "after_string": "TimesRetired",
                "offset": 26,
                "type": "int32",
                "occurrence": 4
            },
            "dd_progress": {
                "after_string": "NormalSave",
                "offset": 100,
                "type": "int32"
            },
            "edd_progress": {
                "after_string": "EliteSave",
                "offset": 100,
                "type": "int32"
            },
            "credits": {
                "after_string": "Credits",
                "offset": 26,
                "type": "int32"
            },
            "bismor": {
                "after_string": "OwnedResources",
                "type": "float32",
                "match_bytes": bytes([ 0xAF, 0x0D, 0xC4, 0xFE, 0x83, 0x61, 0xBB, 0x48, 0xB3, 0x2C, 0x92, 0xCC, 0x97, 0xE2, 0x1D, 0xE7 ])
            },
            "enor pearl": {
                "after_string": "OwnedResources",
                "type": "float32",
                "match_bytes": bytes([ 0x48, 0x8D, 0x05, 0x14, 0x6F, 0x5F, 0x75, 0x4B, 0xA3, 0xD4, 0x61, 0x0D, 0x08, 0xC0, 0x60, 0x3E ])
            },
            "jadiz": {
                "after_string": "OwnedResources",
                "type": "float32",
                "match_bytes": bytes([ 0x22, 0xBC, 0x4F, 0x7D, 0x07, 0xD1, 0x3E, 0x43, 0xBF, 0xCA, 0x81, 0xBD, 0x9C, 0x14, 0xB1, 0xAF ])
            },
            "magnite": {
                "after_string": "OwnedResources",
                "type": "float32",
                "match_bytes": bytes([ 0xAA, 0xDE, 0xD8, 0x76, 0x6C, 0x22, 0x7D, 0x40, 0x80, 0x32, 0xAF, 0xD1, 0x8D, 0x63, 0x56, 0x1E ])
            },
            "umanite": {
                "after_string": "OwnedResources",
                "type": "float32",
                "match_bytes": bytes([ 0x5F, 0x2B, 0xCF, 0x83, 0x47, 0x76, 0x0A, 0x42, 0xA2, 0x3B, 0x6E, 0xDC, 0x07, 0xC0, 0x94, 0x1D ])
            },
            "croppa": {
                "after_string": "OwnedResources",
                "type": "float32",
                "match_bytes": bytes([ 0x8A, 0xA7, 0xFB, 0x43, 0x29, 0x3A, 0x0B, 0x49, 0xB8, 0xBE, 0x42, 0xFF, 0xE0, 0x68, 0xA4, 0x4C ])
            },
            "barley bulb": {
                "after_string": "OwnedResources",
                "type": "float32",
                "match_bytes": bytes(
                    [0x22, 0xDA, 0xA7, 0x57, 0xAD, 0x7A, 0x80, 0x49, 0x89, 0x1B, 0x17, 0xED, 0xCC, 0x2F, 0xE0, 0x98])
            },
            "yeast cone": {
                "after_string": "OwnedResources",
                "type": "float32",
                "match_bytes": bytes(
                    [0x07, 0x85, 0x48, 0xB9, 0x32, 0x32, 0xC0, 0x40, 0x85, 0xF8, 0x92, 0xE0, 0x84, 0xA7, 0x41, 0x00])
            },
            "malt star": {
                "after_string": "OwnedResources",
                "type": "float32",
                "match_bytes": bytes(
                    [0x41, 0xEA, 0x55, 0x0C, 0x1D, 0x46, 0xC5, 0x4B, 0xBE, 0x2E, 0x9C, 0xA5, 0xA7, 0xAC, 0xCB, 0x06])
            },
            "starch nut": {
                "after_string": "OwnedResources",
                "type": "float32",
                "match_bytes": bytes(
                    [0x72, 0x31, 0x22, 0x04, 0xE2, 0x87, 0xBC, 0x41, 0x81, 0x55, 0x40, 0xA0, 0xCF, 0x88, 0x12, 0x80])
            }
        }

    def get_property(self, name):
        if name not in self.drg_properties:
            raise ValueError(f"Unknown property {name}")
        drg_property = self.drg_properties[name]

        occurrence = drg_property.get("occurrence", 1)

        position = 0

        sliced_data = self.binary

        for i in range(occurrence):
            position += sliced_data.index(drg_property["after_string"].encode(encoding='utf-8')) + 1
            sliced_data = self.binary[position+1:]

        if occurrence == 1:
            position -= 1

        position += len(drg_property["after_string"])
        position += drg_property.get("offset", 0)

        match_bytes = drg_property.get("match_bytes", [])
        if match_bytes:
            position += sliced_data.index(drg_property["match_bytes"]) + 4

        value = None
        if drg_property["type"] == "int32":
            value = struct.unpack("i", self.binary[position:position + 4])[0]
        elif drg_property["type"] == "float32":
            value = struct.unpack("f", self.binary[position:position + 4])[0]

        return value

    def get_converted_level_for(self, drg_class):
        raw_xp = self.get_property(drg_class+"_xp")
        level = 0
        while constants.level_breakpoints[level] <= raw_xp:
            level += 1

        remainder_xp = raw_xp - constants.level_breakpoints[level-1]
        current_xp_bar = constants.level_breakpoints[level] - constants.level_breakpoints[level-1]
        if level >= 25:
            current_xp_bar = 100
            remainder_xp = 100

        progress = remainder_xp/current_xp_bar*100
        return level, remainder_xp, current_xp_bar, progress

    def get_formatted_stars(self, drg_class):
        star_count = self.get_property(drg_class+"_stars")
        color = "${color white}"

        if star_count == 0:
            star_message = "No stars yet!"
        elif star_count <= 3:
            color = "${color #c2775e}"
            star_message = f"{star_count} Bronze Stars"
        elif star_count <= 6:
            color = "${color #a2a1a6}"
            star_message = f"{star_count-3} Silver Stars"
        elif star_count <= 9:
            color = "${color #fbc23a}"
            star_message = f"{star_count-6} Gold Stars"
        elif star_count <= 12:
            color = "${color #ffefac}"
            star_message = f"{star_count-9} Platinum Stars"
        elif star_count <= 15:
            color = "${color #1bd8e1}"
            star_message = f"{star_count-12} Diamond Stars"
        else:
            color = "${color #ef5a2d}"
            star_message = f"{star_count-15} Legendary Stars"

        return f"{color}{star_message}"

    def get_promotion_costs(self):
        mineral_costs = {mineral: 0 for mineral in constants.mineral_names}
        mineral_costs["Credits"] = 0
        for drg_class in ["gunner", "driller", "scout", "engineer"]:
            star_count = self.get_property(drg_class+"_stars")
            promotion_cost = constants.promotion_costs[drg_class][star_count]
            for mineral, amount in promotion_cost.items():
                if mineral not in mineral_costs or mineral_costs[mineral] < amount:
                    mineral_costs[mineral] = amount

        return mineral_costs

    def print_formatted_dd_info(self):
        print("${color white}${alignc}--------------------Deep Dives--------------------")

        original_reset_time = datetime.datetime.fromisoformat('2021-09-23 11:00:00+00:00')
        now_utc = datetime.datetime.now(datetime.timezone.utc)
        days_since_reset_time = (now_utc - original_reset_time).days
        days_to_last_reset = days_since_reset_time//7 * 7

        last_weekly_reset = original_reset_time + datetime.timedelta(days=days_to_last_reset)
        next_weekly_reset = original_reset_time + datetime.timedelta(days=days_to_last_reset+7)

        reset_in = next_weekly_reset-now_utc

        if last_weekly_reset > self.last_modified:
            print("${color white} !!! NEW DEEP DIVES AVAILABLE !!!")
            print("${color yellow} You'll miss them in "+format_timedelta(reset_in))
        else:
            print("${color white} New Deep Dives in " + format_timedelta(reset_in))
            dd_names = {
                "dd": "Deep Dive",
                "edd": "Elite Deep Dive"
            }
            for dd_type in ["dd", "edd"]:
                raw_status = self.get_property(dd_type + "_progress")
                if raw_status == 3:
                    status = "${color green}3/3 COMPLETED"
                elif raw_status < 3:
                    status = "${color yellow}" + str(raw_status) + "/3 stages"
                else:
                    status = "${color red}Not yet attempted"
                print("${color white} " + dd_names[dd_type] + ": " + status)


class_colors = {
    "gunner": "green",
    "engineer": "red",
    "driller": "yellow",
    "scout": "#4444FF"
}

if __name__ == "__main__":
    save = DRGSaveFile(sys.argv[1])
    if sys.argv[2] == "level":
        print(save.get_converted_level_for(sys.argv[3])[0])
    elif sys.argv[2] == "xp":
        print(save.get_converted_level_for(sys.argv[3])[1])
    elif sys.argv[2] == "level_progress":
        print(save.get_converted_level_for(sys.argv[3])[3])
    elif sys.argv[2] == "stars":
        print(save.get_property(sys.argv[3]+"_stars"))
    elif sys.argv[2] == "formatted_stars":
        print(save.get_formatted_stars(sys.argv[3]))
    elif sys.argv[2] == "conky_formatted":
        have_not_played_for = datetime.datetime.now(datetime.timezone.utc) - save.last_modified
        if have_not_played_for.seconds > 3600 or have_not_played_for.days > 0:
            print("${alignc}${color white} You haven't played DRG in "+format_timedelta(have_not_played_for))
        else:
            print("${alignc}${color green} FOR ROCK AND STONE!")

        print("${color white}${alignc}---------------------Classes---------------------")
        for drg_class in ["Gunner", "Engineer", "Driller", "Scout"]:
            lower_name = drg_class.lower()
            class_color = class_colors[lower_name]
            print("${color white}"+drg_class+" "+save.get_formatted_stars(lower_name))
            level, xp, total_xp, progress = save.get_converted_level_for(lower_name)
            raw_xp = save.get_property(lower_name+"_xp")
            total_progress = raw_xp/315000*100
            print("${color " + class_color + "} Level " + str(level) + "${alignr}" + f"{315000-raw_xp} XP left ({total_progress:.1f}%)")
        print("${color white}${alignc}---------------------Profile---------------------")
        promotion_costs = save.get_promotion_costs()

        for mineral, color in zip(constants.mineral_names, constants.mineral_colors):
            mineral_count = int(save.get_property(mineral.lower()))
            mineral_needed = promotion_costs[mineral]
            if mineral_count < mineral_needed:
                needed_str = f"{mineral_needed - mineral_count} more needed ({mineral_count/mineral_needed*100:.2f}%)"
            else:
                needed_str = f"{mineral_count - mineral_needed} to spare"
            print(f"{color}{mineral}: {mineral_count} "+"${alignr}"+needed_str)

        print("${color pink}", end="")
        for ingredient, color in zip(constants.ingredient_names, constants.ingredient_colors):
            ingredient_count = int(save.get_property(ingredient.lower()))
            print(f"{color}{ingredient}: {ingredient_count}")

        # print("${color white}Credits: ${color yellow}"+str(save.get_property("credits"))+"")
        # print("${color #00d310} Jadiz: " + str(int(save.get_property("jadiz"))))
        # print("${color #ffe008} Bismor: "+str(int(save.get_property("bismor"))))
        # print("${color #e94002} Magnite: " + str(int(save.get_property("magnite"))))
        # print("${color #5ae551} Umanite: " + str(int(save.get_property("umanite"))))
        # print("${color #f7f7f7} Enor Pearl: " + str(int(save.get_property("enor pearl"))))
        # print("${color #62cea7} Croppa: " + str(int(save.get_property("croppa"))))

        save.print_formatted_dd_info()
    else:
        print(f"{sys.argv[2]}: {save.get_property(sys.argv[2])}")
