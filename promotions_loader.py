# This script eats a copy-and-paste of the table on https://deeprockgalactic.fandom.com/wiki/Promotion
# and spits out a nice dict to be used in the main tool
import json


def get_middle_number(string):
    split = string.split()
    return int(split[int(len(split)/2)])


def get_resource_name(string):
    split = string.split()
    return " ".join(split[int(len(split) / 2)+1:])


if __name__ == "__main__":
    result = {
        0: [],
        1: [],
        2: [],
        3: []
    }
    with open("Promotions") as promotions_file:
        current_tier = None
        current_tier_count = -1
        line_in_class = 0
        for line in promotions_file:
            line_in_class += 1
            if line.endswith("I\n"):
                current_tier_count += 1
                current_tier = line[:-1]
                for i in range(4):
                    result[i].append({})
                line_in_class = 0
                current_class = 0

            if 1 <= line_in_class <= 4:
                result[current_class][current_tier_count][get_resource_name(line)] = get_middle_number(line)
            if line_in_class == 5:
                line_in_class = 0
                current_class += 1

    print(result)
    result["driller"] = result[0]
    result["engineer"] = result[1]
    result["gunner"] = result[2]
    result["scout"] = result[3]
    del result[0]
    del result[1]
    del result[2]
    del result[3]

    with open("promotion_costs.json", mode="w") as json_file:
        json.dump(result, json_file)
